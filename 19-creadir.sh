#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# 
#---------------------
# almenys un argument
ERR_ARGS=1
ERR_MKDIR=2
status=0

if [ $# -ne 1 ]
then
  echo "Error:  d'arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_ARGS
fi

# 
if [ ]
  echo ""

# retornar
for nom in $1
do
  mkdir $nom &>/dev/null
  if [ $? -ne 0 ]
  then
    echo "Error: no s'ha creat $nom" >&2
	status=$ERR_MKDIR
  fi
done
exit $status


