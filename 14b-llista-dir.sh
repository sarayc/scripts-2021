#! /bin/bash
# @ Saray ASIX-M01 Curs 2021-2022
# Febrer
# validar si és un directori
#----------------------------------------------------
ERR_ARGS=1
ERR_NODIR=2
# si numero arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "Usage: $0 dir "
  exit $ERR_ARGS
fi

# si no és un directori plegar
if ! [ -d $1 ]
then
  echo "Error: $1 no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi
# fer un ls del directori
llista_dir=$(ls $dir)
num=1
for nom in $llista_dir
do
  echo "$num: $nom"
  ((num++))
done
exit 0
 



