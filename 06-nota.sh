#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# validar si està aprovat o suspès
#    $ programa validar nota
# validar la nota entre un suspens, aprovat, excel·lent o notable
#------------------
ERR_ARGS=1
ERR_NOTA=2
# 1) si num arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_ARGS
fi 

# 2) validar rang nota [0-10] 
if ! [ $1 -ge 0 -a $1 -le 10 ]
then 
  echo "Error nota $1 no vàlida"
  echo "nota pren valors de 0 al 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi

# 3) xixa
nota=$1
if [ $nota -lt 5 ]
then 
  echo "la nota $nota és un suspès"
elif [ $nota -lt 7 ]
then 
  echo "$nota aprovat"
elif [ $nota -lt 9 ]
then 
  echo "la nota $nota és un notable"
else
  echo "la nota $nota és un excel·lent "
fi
exit 0

