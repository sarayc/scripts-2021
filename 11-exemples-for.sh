#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle for
#----------------------------------

# llistar tots els logins numerats del etc/passwd
logins=$(cut -d: -f1 /etc/passwd) #si els volguessim ordenar posem|sort 
num=1
for elem in $logins
do
  echo "$num: $elem"
  ((num++))
done
exit 0


# llistar numerats el llistat del directori actiu
llista_noms=$(ls)
num=1
for elem in $llista_noms
do
  echo "$num: $elem"
  ((num++))
done
exit 0


# llistar els arguments numerats (mostrar arguments i ordenar-los)
num=1
for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))
done
exit 0

# iterar per la llista d'arguments
# $@ extandeix els word encara que estiguin encapsulats
for arg in "$@*"
do
  echo $arg
done 
exit 0

# iterar per la llista d'arguments (1 iteració)
for arg in "$*"
do
  echo $arg
done
exit 0

# iterar per la llista d'arguments
for arg in $*
do
  echo $arg
done
exit 0

# iterar pel valor d'una variable # command substitution
llistat=$(ls)  
for nom in $llistat
do 
  echo $nom
done
exit 0


 # iterar per un conjunt d'elements
for nom in "pere pau marta anna"
do
  echo "$nom"
done 
exit 0


# iterar per un conjunt d'elements
for nom in "pere" "pau" "marta" "anna"
do
  echo "$nom"
done
exit 0
