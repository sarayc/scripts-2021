#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# arguments amb for
# descripció: separar opcions i arguments
# $ prog [ -a file -b -c -d num -e ] args...
#---------------------
# si num arguments no és correcte plegar
ERR_ARGS=1
if [ $# -ne 1 ]
then
  echo "Error:  d'arguments incorrecte"
  echo "Usage: $0 [-a -b -c -i -s -r] arg[...]"
  exit $ERR_ARGS
fi

# 
opcions=""
arguments=""
file=""
num=""

# utilitzem el while 
while [ "$1" ]
do
  case $1 in 
    "-b"|"-c"|"e")
      opcions="$opcions $1";;
    "-a")
      opcions="$opcions $1"
      file=$2
      shift;;
      "-d")
      opcions="$opcions $1"
      num=$2
      shift;;
   *) 
      arguments="$arguments $1";;
  esac
  shift
done
echo "opcions: $opcions"
echo "arguments: $arguments"
echo "file: $file"
echo "num: $num"
exit 0
