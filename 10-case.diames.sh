#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# validar dia del mes
#----------------------------------
ERR_NARGS=1
ERR_MES=2
# si num arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "Error:  d'arguments incorrecte"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi

# validar argument pren valors [1-12]
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error, mes $mes no vàlid"
  echo "mes entre [1-12]"
  echo "Usage:$0 mes"
  exit $ERR_MES
fi

# xixa: determinat numero dies del més
case $mes in
  "2")
    dies=28;;

  "4"|"6"|"9"|"11")
    dies=30;;

   *)

    dies=31;;
esac 
echo "El mes: $mes, té $dies dies"
exit 0

