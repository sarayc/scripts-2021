#! /bin/bash
# @ Saray ASIX-M01 Curs 2021-2022
# Febrer
# validar si és un directori
#----------------------------------------------------
ERR_ARGS=1
ERR_NODIR=2
# si numero arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "ERROR: num args incorrecte"
  echo "Usage: $0 dir "
  exit $ERR_ARGS
fi

# si no és un directori plegar
if ! [ -d $1 ]
then
  echo "Error: $1 no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi
# fer un ls del directori
dir=$1
num=1
for dir in $*
do
  if ! [ -d $dir ]
  then
	echo "Error: $dir no és un directori" 1>&2
  else 
	llista_dir=$(ls dir)


for nom in $llista_dir
do
    if [ -h "$dir/$nom" ]
    then
      echo "$nom és un simbòlic link"
	elif [ -d "$dir/$nom" ]
	then
      echo "$nom és un directori"
    elif [ -f "$dir/$nom" ]
    then
      echo "$nom és un regular file"
    else
      echo "$nom és un altra cosa"
    fi
done
exit 0
