#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# validar si està aprovat o suspès
#    $ programa fitxers
#------------------
ERR_ARGS=1
ERR_NODIR=2
# 1) si num arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "Error:  d'arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_ARGS
fi

# si es demana help mostrar i plegar
if [ "$1" = "-h" -o "$1" = "--help" ]
then
  echo "Programa: $0 dir"
  echo "Autor: @saray"
  echo "that's all folks"
  exit 0
fi
# 2) si no és un directori plegar
if ! [ -d $1 ]
then
  echo "Error: $1 no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi

# 3) xixa
dir=$1
ls $dir
exit 0
