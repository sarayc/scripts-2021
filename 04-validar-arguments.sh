#! /bin/bash
# @ Saray ASIX-M01 Curs 2021-2022
# Validar que té exàctament 2 args i mostrar-los
#----------------------------------------------------
# 1) si numero arguments no és correcte plegar
if [ $# -ne 2 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog arg1 arg2"
  exit 1
fi

# xixa
echo "Els dos args son: $1, $2 "
exit 0

