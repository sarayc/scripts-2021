#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# validar notes
#---------------------
ERR_NARGS=1
ERR_NOTA=2
# si num arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "Error:  d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi

# iterar per cada nota
for nota in $*
do
  if ! [ $nota -ge 0 -a $nota -le 10 ]
  then
    echo "Error: nota  $nota no vàlid [0,10]" >> /dev/stderr
  elif [ $nota -lt 5 ]
  then 
	echo "la nota $nota és un suspès"
  elif [ $nota -lt 7 ]
  then 
	echo "la nota $nota és un aprovat"
  elif [ $nota -lt 9 ]
  then 
	echo "la nota $nota és un notable"
  else
	echo "la nota $nota és un excel·lent"
  fi
done
exit 0

