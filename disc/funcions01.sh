#! /bin/bash
# @edt curs 2021-2022
# ASIX M01-ISO
# Exemples de funcions
#----------------------------
function hola(){
		echo "hola"
		return 0
}

function dia(){
		date
		return 0
}

function suma(){
		echo $(($1+$2))
		return 0
}

function all(){
		hola
		echo "avui som: $(dia)"
		suma 6 9 
}
# donat un login calcular amb du l'ocupació del home de l'usuari
# cal obteir el home del /etc/passwd
function fsize(){
	# 1) validar si existeix login 
	login=$1
	linia=$(grep "^$login:" /etc/passwd)
	if [ -z "$linia" ]
	then
		echo "Error: user $login inexistent"
		return 1
	fi

	# 2) obtenir el home
 	dirHome=$(echo $linia | cut -d: -f6)	
	du -sh $dirHome
	
	# 3) calcular du -sh
	du -sh $dirHome
	return 0
}

# rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize.
function loginargs(){
	# 1) validar almenys es rep 1 login

	# 2) iterar per cada login
		# 2a) per cada login aplicar fsize

	# 3) login file
	# rep com a argument un nom de fitxer que conté un login per linia.
	# mostrar l'ocupació de disc de cada usuari usant fsize. Verificar que es rep un argument i que és un regular file

}	

function loginfile(){
		# validar si existeix fitxer
		if [ ! -f "$1" ] ;
		then
			echo "fitxer inexistent"
			return 1
		fi
		# procesar fitxer
		fileIn=$1
		while read -r line
		do
			echo $line
		done < fileIn 
}
# loginboth procesa file o stdin, mostra fsize dels users
function loginboth(){
	fileIn=/dev/stdin
	if [ $# -eq 1 ];
	then
		fileIn=$1
	fi
	while read -r login
		do
			fsize $login
		done < $fileIn
}

#5) validar rep un arg validar que es un gid vàlid i retorna la llista de loguins que tenen aquest grup com a grup principal
function grepgid(){
	# grep "^[^:]*:[^:]:$GID:"
	# cut -d: -f1,4 /etc/passwd | grep ":1000$"
	if [ $# -ne 1 ];
	then
		echo " no és vàlid"
		return 1
	fi
	gid=$1
	grep -q "^[^:]*:[^:]:$gid:" /etc/group
	if [ $? -ne 0];
	then
		echo "error gid $gid no existeix"
		return 2
	fi
	cut -d: -f1,4 /etc/passwd | grep ":$gid$" | cut -d: -f1
}


#6) donat un gid com argument buscar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home
function gidsize(){
	gid=$1
	llista_logins=$(grepgid $gid)
	for login in $llista_logins
	do
		fsize $login
	done
}

#7) informe de tots els gids llistant l'ocupació dels usuaris que hi pertanyen
function allgidsize(){
	llista_gids=$(cut -d: -f4 /etc/passwd | sort -uh)
	for gid in $llista_gids
	do
		echo "GID: $gid------------"
		gidsize $gid
	done
}

#8) filter group
# llista linies del /etc/passwd dels usuaris del grup [0-100]
function filterGroup(){
	while read -r line
	do
		gid=$(echo $line | cut -d: -f4)
		if [ $gid -ge 0 -a $gid -le 100 ];
		then
			echo $line
		fi
	done < $file
}

