#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle while
#------------------

# numerar stdin linia a linia i passar a majúscules
num=1
while read -r line
do
  echo "$num: $line" | tr 'a-z''A-Z'
  ((num++))
done
exit 0

# processar stdin fins al token FI
read -r line
while [ "$line" != "FI" ]
do
  echo "$line"
  read -r line
done 
exit 0
		
# processar l'entrada estàndard i numerar stdin línia a línia
num=1
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

# processar l'entrada estandard
# línia a línia
while read -r line
do
  echo $line
done
exit 0

# iterar arguments amb shift( mostrar els arguments utilitzant while)
while [ -n "$1" ]
do
  echo "$1,$#,$*"
  shift
done
exit 0

# comptador decreixent del arg [N-0]
num=$1
MIN=0
while [ $num -ge $MIN ]
do
  echo -n "$num, "
  ((num--))
done
exit 0

# mostrar un comptador del 1 al MAX
numero=1
MAX=10
while [ $numero -le $MAX ] 
do
 echo "$numero"
 ((numero++))
done
exit 0
