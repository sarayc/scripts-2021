#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# validar si està aprovat o suspès
#    $ programa tipus file
#----------------------------------
ERR_ARGS=1
ERR_NOEXIST=2
# 1) si num arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "Error:  d'arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_ARGS
fi

# 2) comprovar si és un regular file, un directori o un link
fit=$1
if [ ! -e $fit ]
then
  echo "$fit no existeix"
  exit $ERR_NOEXIST
elif [ -f $fit ] 
then
  echo "$fit és un regular file"	
elif [ -L $fit ]
then
  echo "$fit és un simbòlic link"
elif [ -d $fit ]
then
  echo "$fit és un directori"
else 
  echo "$fit és un altra cosa"
fi
exit 0
