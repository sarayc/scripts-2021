#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# processar arguments que són matricules:
# a) llistar les vàlides del tipus: 9999-AAA
# b) stdout les que són vàlides, per stderr les no vàlides. Retoma de status el número d'errors (de no vàlides).

# processar arguments: for
num=0
for arg in $@
do
  echo $arg | sed -r 's//^[0-9]{4}-[A-Z]{3}$//' 2>/dev/null
  echo $arg | grep -e "^[0-9]{4}-[A-Z]{3}$" 
  if [ $? -ne 0 ]
  then
    echo "Error: $arg" >> /dev/stderr
    ((num++))
  fi
done
exit $num
