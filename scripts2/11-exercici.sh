#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# idem anterior però rep els GIDs com a arguments de la línia d'ordres.
status=0
for gid in $*
do
  fitxer=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ $? -eq 0 ]
  then 
    logins=$(echo $fitxer | cut -d: -f1 | tr '[a-z]' '[A-Z]')
    gids=$(echo $fitxer | cut -d: -f4 | tr '[a-z]' '[A-Z]')
   echo "login: $login, gid: $gid, gids $gids"    
 else
  echo "Error $git no existeix" >> /dev/stderr
  status=1  
  fi
done
exit $status

