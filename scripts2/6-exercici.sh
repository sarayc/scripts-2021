#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# processar per stdin línies d'entrada tipus "Tom Snyder" i mostrar per stdout la línia en format ------------> T.Snyder.
# processar per stdin: whhile
while read -r line
do
  noms=$(echo $line | cut -c1)
  cognom=$(echo $line | cut -d ' ' -f2)
  echo $noms.$cognom
done
exit 0
