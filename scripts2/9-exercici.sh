#! /bin/bash
# @saray asix m01 2021-2022
# març
# programa: prof.sh [-r -m -c cognom -j -e edat] arg...
#------------------------------------
# escriure el codi que desa en les variables: opcions, cognom, edat i arguments els valors corresponents.
# no cal validar ni mostrar res!
# Per exemple si es crida: $prog.sh -e 18 -r -c puig -j wheel postgres ldap
# retona: opcions «-r -j», cognom «puig», edat «18», arguments «wheel postgres ldap»

# arguments: for
opcions=""
cognom=""
edat=""
arguments=""

while [ $1 ]
do
  case $1 in
    "-r"|"-m"|"-j")
	  opcions="$opcions $1";;
    "-c")
	  cognom="$cognom $1"
	  shift;;
    "-e")
	  edat="$edat $1"
	  shift;;
     *) 
	  arguments="$arguments $1";;
  esac
  shift
done
echo "El cognom: $cognom"
echo "L'edat: $edat"
echo "Les opcions: $opcions"
echo "Els arguments: $arguments"


