#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# rep per stdin GIDs i llista per stdout la informació de cada un d'aquests grups, en format: gname:GNAME, gid: GID, users : USERS
# stdin: while
status=0
while read -r gid
do
  fitxer=$(grep "^[^:]*:[^:]*:$gid:" /etc/group)
  if [ $? -eq 0 ]
  then
    gname=$(echo $fitxer | cut -d: -f1 | tr '[a-z]' '[A-Z]')
    users=$(echo $fitxer | cut -d: -f4 | tr '[a-z]' '[A-Z]')
    echo "$gname: $gname, gid: $gid, users $users"
  else
  echo "Error $git no existeix" >> /dev/stderr
  status=1
  fi
done
exit $status
