#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# processar stdin mostrant per stdout les línies de menys de 50 caràcters
# processar per stdin: while
while read -r line
do
  caracters=$(echo $line | wc -c)
  if [ $caracters -lt 50 ]
  then
    echo $line
  fi
done
exit 0
