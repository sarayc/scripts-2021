#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# processar stdin mostrant per stdout les línies numerades i en majúscules.
# processar entrada estàndard: while 
num=0
while read -r line
do
  echo $num : $line | tr '[:lower:]' '[:upper:]'
  ((num++))
done
exit 0
