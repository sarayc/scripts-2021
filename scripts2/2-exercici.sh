#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# processar els arguments i comptar quantes n'hi ha de 3 o més caràcters
# processar arguments: for
num=0
for arg in $*
do
  caracters=$(echo $arg | wc -c)
  if [ $caracters -gt 3 ]
  then
    ((num++))
  fi
done
echo " tenim $caracters de 3 o més caràcters"
exit 0
