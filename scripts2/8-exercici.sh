#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# programa: prog file
# validar existeix almenys un file. Per a cada file comprimir-lo. Generar per stdout el nom del file comprimit si s'ha comprimit correctament, o un missatge d'error per stderror si no s'ha pogut comprimir. En finalitzar es mostra per stdout quants files ha comprimit.
# retorna status 0 ok, 1 error nº arguments, 2 si algun error en comprimir.

ERR_ARGS=0
# validar existeix almenys un file
if [ $# -eq 0 ]
then
  echo "Error: número d'arguments incorrecte, almenys has de posar 1 file"
  echo "Usage: $0 prog file[...]"
  exit $ERR_ARGS
fi

# -h p --help
OK=0
if [ $1 = "-h" -o $1 = "--help" ]
then
  echo "asix 21/22"
  echo "usage: prog file[...]"
  exit $0
fi

# per a cada file comprimir
cont=0
status=0
for file in $*
do
  if [ -f $file ]
  then
    gzip $file >> /dev/null
    if [ $? -eq 0 ]
    then 
      echo "$file s'ha comprimit correctament"
      ((cont++))
    else
      echo "error $file" >> /dev/stderr
      status=2
    fi
  fi
done
echo "s'han comprimit $cont fitxers"
exit $status 

