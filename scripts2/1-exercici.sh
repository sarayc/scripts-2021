#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# processar els arguments i mostrar per stdout només els de 4 o més caràcters.
# processar arguments: for

for arg in $*
do 
  caracters=$(echo $arg | wc -c)
  if [ $caracters -ge 4 ]
  then 
    echo $arg
  fi
done
exit 0

