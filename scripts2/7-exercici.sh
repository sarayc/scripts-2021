#! /bin/bash
# @saray ASIX curs 2021-2022
# març
#
#-------------------------------------------
# programa: prog -f|-d arg1 arg2 arg3 arg4
# a) valida que els quatre arguments rebuts són tots del tipus que indica el flag. És a dir, que si es crida amb -f valida que tots quatre són file. Si es crida amb -d valida que tots quatre són directoris.
# Retorna 0 OK, 1 error nº arguments, 2 hi ha elements errònis.
# b) aplicar amb el cas: prog -h|--help
status=0
ERR_ARGS=0
# validar número d'arguments
if [ $# -ne 5 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 -f | -d arg1 arg2 arg3 arg4"
  exit $ERR_ARGS
fi

# validar que sigui un fitxer o un directori
if [ "$1" != "-f" -a "$1" != "-d" ]
then
  echo "Error: format no correcte"
  echo "Usage: $0 -f | -d arg1 arg2 arg3 arg4"
  exit $ERR_ARGS
fi

# valors de retorn
# arguments: for
tipus=$1
shift
for arg in $*
do
  if ! [ $tipus $arg ]
  then
    status=2
 
  fi
done
exit $status
  



