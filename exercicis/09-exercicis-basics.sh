#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
#
#------------------------------------
# Fer un programa que rep per stdin noms d’usuari (un per línia), si existeixen en el sistema (en el fitxer /etc/passwd) mostra el nom per stdout. Si no existeix el mostra per stderr.



# rep per arguments
noms=$(cut -d: -f1 /etc/passwd)
while read -r arg
do
  echo $noms | grep -wq $arg
  if [ $? -ne 0 ]
  then
    echo "Error: $arg" >> /dev/stderr

  else
    echo $arg
  fi
  #
done
exit 0

