#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle while
#------------------------------------
# mostrar linia a linia l'entrada estàndard, retallant només els primers 50 caràcters 

# linia a linia

while read -r line
do
  echo $line | cut -c1-50 
  
done
exit 0



