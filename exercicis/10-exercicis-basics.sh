#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
#
#------------------
# Fer un programa que rep com a argument un número indicatiu del número màxim de línies a mostrar. El programa processa stdin línia a línia i mostra numerades un màxim de num línies.

# validar numero arguments
ERR_ARGS=1
# validar numeros arguments
if [ $# -ne 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 numero"
  exit $ERR_ARGS
fi

# 
num=1
max=$1
while read -r line
do 
  echo "$num: $line"
  if [ $num -ge $max ]
  then
	exit 0
  fi
  ((num++))
done
