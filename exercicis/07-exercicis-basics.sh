#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
#
#------------------------------------
# processar linia a linia l'entrada estandard, si la línia té mes de 60 caracters la mostra, si no no.

# linia a linia

while read -r line
do
  caracters=$(echo $line | wc -c)
  if [ $caracters -gt 60 ]
  then
    echo "$line"
  fi	
done
exit 0
