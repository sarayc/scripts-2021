#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle while
#------------------------------------
# Fer un programa que rep com a arguments números de més (un o més) i indica per a cada mes rebut quants dies té el mes. 

ERR_NARGS=1
# 1) si num args no és correcte plegar
if [ $# -eq 0 ]
then
   echo "Error: número d'arguments incorrecte"
   echo "Usage: $0 mes"
   exit $ERR_NARG
fi
mes=$1
# xixa: determinar numero dies del mes

for mes in $*
do
  if ! [ $mes -ge 1 -a $mes -le 12 ]
  then 
	echo " Error, mes $mes no vàlid" >> /dev/sterr

  else
      case $mes in
       "2") 
         dies=28;;
      "4"|"6"|"9"|"11")
         dies=30;;
      *) 
	     dies=31;;
      esac
  echo "El mes: $mes, té $dies dies"
  fi
done
exit 0 

