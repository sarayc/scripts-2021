#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle while
#------------------------------------
# mostrar arguments rebuts linia a linia, numerànt-los
num=1
while [ -n "$1" ]
do
  echo "$num:$1"
  shift
  ((num++))
done
exit 0 


# for
num=1
for arg in $*
do
  echo "$num: $arg"
  ((num++))
done
exit 0
