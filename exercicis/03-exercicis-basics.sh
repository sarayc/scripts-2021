#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle while
#------------------------------------
# Fer un comptador des de 0 fins al valor indicat per l'argument rebut 
num=0
MAX=$1
while [ $num -le $MAX ]
do
  echo "$num"
  ((num++))
done
exit 0




