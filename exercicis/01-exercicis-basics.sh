#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
# Descripcio: exemples bucle while
#------------------
# mostra l'entrada estandard numerant linia a linia 
num=1
# linia a linia
while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0

