#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
#
#------------------------------------
# programa que rep com a argument noms d'usuaris, si existeixen en el sistema mostra el nom per stdout.si no existeix el mostra per stderr.

ERR_ARGS=1
# validar numeros arguments
if [ $# -lt 1 ]
then  
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 noms d'usuaris"
  exit $ERR_ARGS
fi 

# rep per arguments
noms=$(cut -d: -f1 /etc/passwd)
for arg in $*
do
  echo $noms | grep -wq $arg
  if [ $? -ne 0 ]
  then
    echo "Error: $arg" >> /dev/stderr

  else 
    echo $arg 
  fi	
  # 
done
exit 0
