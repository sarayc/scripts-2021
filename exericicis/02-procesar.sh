#! /bin/bash
# @saray ASIX M01 Curs 2021-2022
# Febrer 2022
# 
#------------------------------------
# processar els arguments i comptar quantes n'hi ha de 3 o més caràcters.

ERR_ARGS=1
# validar numeros argumens
# si num arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "Error:  d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_ARGS
fi


# rep per arguments
num=0
for $arg in $*
do
  caracters=$(echo $arg | wc -c)
  if [ $caracters -gt 3 ]
  then
    ((num++))
  echo "$num:$arg"
  fi
done
  echo "$num : $arg"
exit 0


