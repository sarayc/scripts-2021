#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# validar si està aprovat o suspès
#    $ programa validar nota
#------------------
ERR_NARGS=1
ERR_NOTA=2
# 1) si num arguments no és correcte plegar
if [ $# -ne 1 ]
then
  echo "Error: numero d'arguments incorrecte"
  echo "Usage: $0 nota"
  exit $ERR_NARGS
fi 
# 2) validar rang nota
if ! [ $1 -ge 0 -a $1 -le 10 ]
then 
  echo "Error nota $1 no vàlida"
  echo "nota pren valors de 0 al 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi


# 3)xixa
nota=$1
if [ $nota -ge 5 ]
then 
  "echo $nota està aprovat"
else
  "echo $nota no està aprovat"
fi
exit 0


