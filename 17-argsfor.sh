#! /bin/bash
# @saray ASIX M01
# Febrer 2022
# arguments amb for
# iterar tots els camps des del primer fins el penúltim
#---------------------
# si num arguments no és correcte plegar
ERR_ARGS=1
if [ $# -ne 1 ]
then
  echo "Error:  d'arguments incorrecte"
  echo "Usage: $0 [-a -b -c -i -s -r] arg[...]"
  exit $ERR_ARGS
fi

# 
opcions=""
arguments=""
# recorrem arguments a argument
for arg in $*
do
  # validar si es una lletra de les opcions o no
  case $arg in
		  "-a"|"-b"|"-c"|"-i"|"-s"|"-r")
		     opcions="$opcions $arg";;
		*)
	         arguments="$arguments $arg";;
  esac
done 
echo"opcions: $opcions"
echo"arguments: $arguments"
exit 0
